const axios = require('axios');

/**
 * get current time
 *
 * check examples.js for usage example
 *
 * @param {string} timezone
 * @returns {Promise<number>}
 */
module.exports = function time(timezone = 'est') {
  const apiUrl = `http://worldclockapi.com/api/json/${timezone}/now`;
  return axios.get(apiUrl).then(res => res.data.currentFileTime);
};
