/**
 * sum numbers given as input
 *
 * check examples.js for usage example
 *
 * @param {number}
 * @param {number}
 * @returns {number}
 */
module.exports = function sum(a, b) {
  if (typeof a !== 'number' || typeof b !== 'number') {
    throw new Error('bad input');
  }
  return a  + b;
};
